var admin = require("firebase-admin");
const fs = require('fs');

// Fetch the service account key JSON file contents
var serviceAccount = require("/Users/Xuan/haobo/work2/cskitty.gitlab.io/data/starmath-cff54-firebase-adminsdk-ox1nr-0683942cf7.json");

// Initialize the app with a service account, granting admin privileges
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://starmath-cff54.firebaseio.com"
});

// As an admin, the app has access to read and write all data, regardless of Security Rules
var db = admin.database();
//var ref = db.ref("restricted_access/secret_document");
var ref = db.ref("/amc");
var data;
ref.once("value", function(snapshot) {
  data = snapshot.val()
  console.log(data);

  fs.writeFile ("../public/questions.json", JSON.stringify(data), function(err) {
    if (err) throw err;
    console.log('complete');
    }
  );
});
