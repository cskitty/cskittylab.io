import requests
import time
import json
from parsel import Selector
from bs4 import BeautifulSoup
import re

url1 = "https://artofproblemsolving.com/wiki/index.php/"
url2 = "_AMC_8_Problems/Problem_"

answer1 = "https://artofproblemsolving.com/wiki/index.php/"
answer2 = "_AMC_8_Answer_Key"

selector1 = "#mw-content-text > div > p:nth-child(2)"
selector2 = "#mw-content-text > div > p:nth-child(3)"
selector3 = "#mw-content-text > div > p:nth-child(4)"
selector4 = "#mw-content-text > div > p:nth-child(5)"
selectors = [selector1, selector2, selector3, selector4]

answerselector = "#mw-content-text > div > ol"

def getQuestion(year, i, questionAnswer):
    amc8url = url1 + str(year) + url2 + str(i)
    print(amc8url)

    response = requests.get(amc8url).text
    selector = Selector(text=response)

    #question = [str(i)]
    question = ""
    for sel in selectors:
        #print("selector ", sel)
        q = selector.css(sel).get()
        #print(q)
        if (q != None):
            #print(q)
            replaceList = []
            for img in re.findall(r'<img\s+src="(.*?)">', q):
                old = '<img src="' + img + '">'
                soup = BeautifulSoup(old, "lxml")
                new = soup.findAll('img')[0]['alt']
                replaceList.append((old,new))
            #print(replaceList)

            for a, b in replaceList:
                if "[asy]" in a:
                    q = q.replace("//latex","https://latex")
                else:
                    q = q.replace(a,b)

            q = q[3:-5]
            while '[asy]' in q:
                s = q.find('[asy]')
                e = q.find('[/asy]')
                q = q[:s-6] + q[e + 6:]
               #print(q)

            #search for answer
            if q.count('qquad') == 4:
                s = q.find('\\textbf')
                if s == -1:
                    s = 2
                answer = q[s - 1:-1].split('qquad')
                choice1 =  answer[0].split()[1].strip().replace('textdollar','$')
                choice2 =  answer[1].split()[1].strip().replace('textdollar','$')
                choice3 =  answer[2].split()[1].strip().replace('textdollar','$')
                choice4 =  answer[3].split()[1].strip().replace('textdollar','$')
                choice5 =  answer[4].split()[1].strip().replace('textdollar','$')

                #remove trailing \\
                ll = []
                for c in [choice1, choice2, choice3, choice4, choice5]:
                    if c.endswith('\\'):
                        c = c[:-1]
                    ll.append(c)
                choice1, choice2, choice3, choice4, choice5 = ll

                answer  = questionAnswer
                #stop here if answer is found
                return question, choice1, choice2, choice3, choice4, choice5, answer
            else:
                question += q.replace('\n','').replace('"','\"').replace('textdollar','$')
    return

def getAnswer(year):
    answerurl = answer1 + str(year) + answer2
    print(answerurl)

    response = requests.get(answerurl).text
    selector = Selector(text=response)

    #print(response)

    q = selector.css(answerselector).get()
    #print(q)

    anslst = q.split("</li>\n<li>")
    #special handling q1, last q
    anslst[0] = anslst[0][-1]
    anslst[-1] = anslst[-1][0]

    #print(anslst)

    for i in range(len(anslst)):
        anslst[i] = ord(anslst[i]) - ord('A') + 1

    #print(anslst)

    return anslst


# Load the bitcoin BIP39 english word dictionary, total 2048 words
#csvfile = open('amc8.csv', 'w', newline='')
#wr = csv.writer(csvfile, dialect='excel')

data = {}
data['amc8'] = []

errorReport = open("errorReport.txt", "w")

def crawl(year):
    answer = getAnswer(year)
    questionCounter = 0

    for i in range(1,26):

        try:
            question, choice1, choice2, choice3, choice4, choice5, result = getQuestion(year, i, answer[i - 1])

        except:
            s = f"Year {year}, Question {i}"
            print(s)
            questionCounter += 1
            errorReport.write(s)
            continue
        #print(question, choice1, choice2, choice3, choice4, choice5, answer)

        data['amc8'].append({
            'q': question,
            'c1': choice1,
            'c2': choice2,
            'c3': choice3,
            'c4': choice4,
            'c5': choice5,
             'a': result,
             'y': year,
             'n': i
        })

    if questionCounter == 0:
        print(f" {year} is a perfect year!!!! \n\n")

#crawl(2000)
for year in range(1999,2019):
    print("Tis the following year of questions is {}".format(year))
    crawl(year)
    #time.sleep(5)

with open('amc8.json', 'w') as outfile:
    json.dump(data, outfile)
