const highScoresList = document.getElementById("highScoresList");
const highScores = JSON.parse(localStorage.getItem("highScores")) || [];

let user = localStorage.getItem('user');
console.log("user=", user);
if (user == null) {
    alert("please sign in first");
    window.location.assign('/index.html');
}


highScoresList.innerHTML = highScores
  .map(score => {
    return `<li class="high-score">${score.name} - ${score.score}</li>`;
  })
  .join("");
