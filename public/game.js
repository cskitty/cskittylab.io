const question = document.getElementById('question');
const choices = Array.from(document.getElementsByClassName('choice-text'));
const progressText = document.getElementById('progressText');
const scoreText = document.getElementById('score');
const progressBarFull = document.getElementById('progressBarFull');
const loader = document.getElementById('loader');
const game = document.getElementById('game');
let currentQuestion = {};
let acceptingAnswers = false;
let score = 0;
let questionCounter = 0;
let availableQuesions = [];

let questions = [];

const config = {
    apiKey: "AIzaSyDbWaToM4bXxQdVcTX3RX3n_EGhzDGfPCo",
    authDomain: "starmath-cff54.firebaseapp.com",
    databaseURL: "https://starmath-cff54.firebaseio.com",
    projectId: "starmath-cff54",
    storageBucket: "starmath-cff54.appspot.com",
    messagingSenderId: "671731703070"
};

const year = document.getElementById('year');
const number = document.getElementById('number');

let user = localStorage.getItem('user');
console.log("user=", user);
if (user == null) {
    alert("please sign in first");
    window.location.assign('/index.html');
}

// Initialize Firebase
firebase.initializeApp(config);

let questionYear =  localStorage.getItem("yearSetting");
if (questionYear == null) {
    questionYear = "1999";
}

let path = "math/amc8/" + questionYear;
let MAX_QUESTIONS = 0;
console.log(path);
firebase.database().ref(path).once('value', (snapshot) => {
    const data = snapshot.val();
    MAX_QUESTIONS = data.length - 1;
    questions = data;
    startGame();
});

//CONSTANTS
const CORRECT_BONUS = 1;

startGame = () => {
    questionCounter = 1;
    score = 0;
    availableQuesions = [...questions];
    getNewQuestion();
    game.classList.remove('hidden');
    loader.classList.add('hidden');
};

getNewQuestion = () => {
    if (availableQuesions.length === 0 || questionCounter >= MAX_QUESTIONS) {
        localStorage.setItem('mostRecentScore', score);
        //go to the end page 
        return window.location.assign('/end.html');
    }

    localStorage.setItem("questionCounter", questionCounter);
    
    progressText.innerText = `Question ${questionCounter}/${MAX_QUESTIONS}`;
    //Update the progress bar
    progressBarFull.style.width = `${(questionCounter / MAX_QUESTIONS) * 100}%`;

    currentQuestion = availableQuesions[questionCounter];
    
    while (currentQuestion == null) {
        currentQuestion = availableQuesions[++questionCounter];

        if (questionCounter >= MAX_QUESTIONS) {
            //go to the end page 
            localStorage.setItem('mostRecentScore', score);
            return window.location.assign('/end.html');
        }
    }
    
	// question.innerText = currentQuestion.question;
	typeset(() => {
		question.innerHTML = currentQuestion.q;

		year.innerHTML = questionYear.toString();
		number.innerHTML = (questionCounter - 1).toString();

		choices.forEach((choice) => {
			const number = choice.dataset['number'];
			choice.innerText = '\\(' + currentQuestion['c' + number] + '\\)' ;
		});

		return question;
	  });


    //availableQuesions.splice(questionIndex, 1);
	acceptingAnswers = true;
	questionCounter++;
}; 

choices.forEach((choice) => {
    choice.addEventListener('click', (e) => {
        if (!acceptingAnswers) return;

        acceptingAnswers = false;
        const selectedChoice = e.target;
        const selectedAnswer = selectedChoice.dataset['number'];

        const classToApply =
            selectedAnswer == currentQuestion.a ? 'correct' : 'incorrect';

        if (classToApply === 'correct') {
            incrementScore(CORRECT_BONUS);
        }

        selectedChoice.parentElement.classList.add(classToApply);

        setTimeout(() => {
            selectedChoice.parentElement.classList.remove(classToApply);
            getNewQuestion();
        }, 1000);
    });
});

incrementScore = (num) => {
    score += num;
    scoreText.innerText = score;
};


let promise = Promise.resolve();

function typeset(code) {
	promise = promise.then(() => {code(); return MathJax.typesetPromise()})
					 .catch((err) => console.log('Typeset failed: ' + err.message));
	return promise;
  }

function restart( ) {
 
}
 