const year = document.getElementById('yearSetting');
const saveYearBtn = document.getElementById('saveYearBtn');

let user = localStorage.getItem('user');
console.log("user=", user);
if (user == null) {
    alert("please sign in first");
    window.location.assign('/index.html');
}

saveYear = () => {
    localStorage.setItem('yearSetting', year.value); 
};  
